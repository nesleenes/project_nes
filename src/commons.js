import axios from 'axios'

const API_HOST = 'https://qt.emeraldtech.co/api';


const alertError = (title, body) => {
    alert(title + ' : ' + body);
};

const formatDigit = (digit) => {
    if (digit < 10) {
        return '0' + digit;
    }
    return digit;
};

const formatDate = (timestamp) => {
    const date = new Date(timestamp);
    return formatDigit(date.getDay()) + '/' +
        formatDigit(date.getMonth()) + '/' +
        formatDigit(date.getFullYear()) + ' ' +
        formatDigit(date.getHours()) + ':' +
        formatDigit(date.getMinutes()) + ':' +
        formatDigit(date.getSeconds());
};

const getErrorDescription = (err) => {
    if (!err) {
        return '';
    }
    if (!err.error_description) {
        return '';
    }
    return 'รหัส : ' + err.error_trace_id + ' วัน ' + formatDate(err.error_at) + ' ที่ ' + err.error_on;
};

const clearFormErrorMessage = () => {
    window.jQuery('.is-invalid').removeClass('is-invalid');
    window.jQuery('.invalid-feedback').text('');
};

const showFormErrorMessage = (fieldName, message) => {
    const $el = window.jQuery('[name=' + fieldName + ']');
    $el.addClass('is-invalid');
    $el.siblings('.invalid-feedback').text(message);
};

axios.interceptors.response.use(resp => {
    clearFormErrorMessage();
    return resp;
}, e => {

    const response = e.response || {};
    const status = response.status;
    const err = response.data || {};

    if (status === 400 || status === 409) {

        clearFormErrorMessage();

        if (err.error === 'invalid_request') {
            const fields = err.error_fields || [];
            const length = fields.length;
            for (let i = 0; i < length; i++) {
                const field = fields[i];
                if (field.code === 'not_null' || field.code === 'not_blank') {
                    showFormErrorMessage(field.name, 'กรุณาระบุ');
                } else {
                    showFormErrorMessage(field.name, field.description);
                }
            }
        } else if (err.error === 'duplicate_name') {
            showFormErrorMessage('name', 'ข้อมูลซ้ำ');
        } else if (err.error === 'duplicate_name') {
            showFormErrorMessage('code', 'ข้อมูลซ้ำ');
        } else if (err.error === 'delete_constraint') {
            alertError(status + ' - ไม่สามารถลบข้อมูลได้', 'เนื่องจากมีข้อมูลอื่น ๆ ใช้งานข้อมูลนี้อยู่ ให้ใช้วิธีเปิด/ปิดการใช้งานแทน');
        } 
    } else if (status === 401) {
        if (err.error_description !== 'User locked') {
            localStorage.removeItem('user_session');
            location.href = "/";
        }
    } else if (status === 403) {
        alertError(status + ' - ไม่มีสิทธิ์เข้าถึง', 'ไม่สามารถใช้งานฟังก์ชันนี้ได้');
    } else if (status === 502 || status === 503) {
        alertError(status + ' - ระบบไม่สามารถใช้งานได้', 'โปรดรอสักครู่ แล้วลองใหม่อีกครั้งในเวลาถัดไป');
    } else if (status == 504) {
        alertError(status + ' - เกตเวย์หมดเวลา (Gateway Timeout)', 'โปรดรอสักครู่ แล้วลองใหม่อีกครั้งในเวลาถัดไป');
    } else if (status === 500) {
        alertError(status + ' - ระบบประมวลผลผิดพลาด', getErrorDescription(err));
    }

    return Promise.reject(e);
});

class Common {

    constructor() {
        this.apiHost = API_HOST;
    }

    /*
    decodeJWT(token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    }
    */

    getApi(path) {
        return axios
            .get(this.apiHost + path, { headers: { 'Content-Type': 'application/json' } })
    }
    postApi(path, data) {
        return axios
            .post(this.apiHost + path, data, { headers: { 'Content-Type': 'application/json' } })
    }
    postApiWithOutData(path) {
        return axios
            .post(this.apiHost + path, { headers: { 'Content-Type': 'application/json' } })
    }
    putApi(path, data) {
        return axios
            .put(this.apiHost + path, data, { headers: { 'Content-Type': 'application/json' } })
    }
    patchApi(path, data) {
        return axios
            .patch(this.apiHost + path, data, { headers: { 'Content-Type': 'application/json' } })
    }
    deleteApi(path) {
        return axios
            .delete(this.apiHost + path, { headers: { 'Content-Type': 'application/json' } })
    }
    download(path) {
        return axios({
            url: this.apiHost + path,
            method: 'GET',
            responseType: 'blob', // important
        })
    }
    uploadFileApi(file, path) {
        const formData = new FormData();
        formData.append("file", file);
        return axios.post(
            this.apiHost + (!!path ? path : "/files/upload"),
            formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        );
    }
}

export default new Common();