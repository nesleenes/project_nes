
import Commons from './commons';

class Authenticate {
    setSession(session) {
        localStorage.setItem('user_session' ,JSON.stringify(session));
    }

    getSession() {
        const str = localStorage.getItem('user_session');
        if (str && str.length) {
            return JSON.parse(str);
        }
        return null;
    }

    hasSession() {
        return !!this.getSession();
    }

    checkExpireToken(token){
        if(Commons.decodeJWT(token).exp < new Date() == true){
            return Commons.decodeJWT(token).exp <= new Date()
        }else{
            return false
        }
    }

    logout() {
        localStorage.removeItem('user_session');
    }

    hasAuthorities(authorities) {
        const session = this.getSession();
        if (!session) {
            return false;
        }
        const userAuthorities = session.user.authorities;
        if (!userAuthorities.length) {
            return false;
        }

        for (let i = 0; i < userAuthorities.length; i++) {
            console.log(userAuthorities[i])
            console.log(authorities.indexOf(userAuthorities[i]))
            if (authorities.indexOf(userAuthorities[i]) >= 0) {
                return true;
            }
        }

        return false;
    }
}

export default new Authenticate;
